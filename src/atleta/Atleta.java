package atleta;

import static java.lang.Thread.sleep;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Atleta extends Thread {

    String nombre;
    Scanner teclado = new Scanner(System.in);

    @Override
    public void run() {
        System.out.println("Ingresa el nombre: ");
        nombre = teclado.nextLine();
        for (int i = 1; i <= 30; i++) {

            System.out.println("Lleva " + i + " km");
            try {
                sleep(1500);
            } catch (InterruptedException ex) {

            }
        }
        System.out.println("¡Ya llegó " + nombre + "!");
    }

    public static void main(String[] args) {
        Atleta at = new Atleta();
        at.start();
    }

}
